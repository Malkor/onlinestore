<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryAddressesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('delivery_addresses', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->timestamps();
      $table->unsignedInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users');
      $table->string('address_title', 60);
      $table->string('first_name', 60);
      $table->string('last_name', 60);
      $table->string('company', 60)->nullable();
      $table->string('address', 90);
      $table->string('address_line_2', 90);
      $table->string('country',60)->nullable();
      $table->string('state',60)->nullable();
      $table->string('city',60)->nullable();
      $table->string('postal_code', 30);
      $table->string('home_phone', 30)->nullable();
      $table->string('mobile_phone', 30)->nullable();
      $table->text('additional_information');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('delivery_addresses');
  }
}
