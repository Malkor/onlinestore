<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->boolean('active')->default(1);
            $table->string('name');
            $table->unsignedInteger('user_id');
            $table->unsignedBigInteger('categorie_id');
            $table->unsignedBigInteger('currency_id')->default(0);

            $table->decimal('price');
            $table->decimal('old_price')->nullable();
            $table->string('video_link')->nullable();
            $table->string('pic')->nullable();

            $table->text('description');

            $table->boolean('top')->default(0);
            $table->boolean('new')->default(0);
            $table->boolean('action_product')->default(0);
            $table->string('lang_id')->nullable();
            $table->softDeletes();

            $table->foreign('categorie_id')
                ->references('id')->on('categories');

            $table->foreign('user_id')
                ->references('id')->on('users');

            $table->foreign('currency_id')
                ->references('id')->on('currencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
