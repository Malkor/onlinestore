<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('users', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name', 50);
      $table->string('email', 50)->unique();
      $table->string('password');
      $table->unsignedInteger('role')->default(0);
      $table->rememberToken();

      $table->string('first_name', 60)->nullable();
      $table->string('last_name', 60)->nullable();
      $table->string('phone', 30)->nullable();
      $table->date('date_of_birth')->nullable();
      $table->string('lang_id', 10)->nullable();
      $table->integer('default_currency')->default(1);
      $table->boolean('special_offers')->default(0);
      $table->boolean('news_letter')->default(0);

      $table->foreign('role')
          ->references('id')->on('roles');

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('users');
  }
}
