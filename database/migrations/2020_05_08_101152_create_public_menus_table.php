<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('public_menus', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default('1');
            $table->string('name');
            $table->string('lang_id')->default('en');
            $table->string('type_menu')->nullable();
            $table->string('route')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('public_menus');
    }
}
