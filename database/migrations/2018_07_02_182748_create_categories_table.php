<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('categories', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('parent_id')->unsigned()->default(0);
      $table->timestamps();
      $table->string('title');
      $table->text('description');
      $table->string('lang_id')->default('en');

      $table->softDeletes();

      /*$table->foreign('parent_id')
        ->references('id')->on('categories');*/
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('categories');
  }
}
