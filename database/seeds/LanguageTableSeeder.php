<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('languages')->insert([
        [
            'name' => 'English',
            'code' => 'en',
        ],
        [
            'name' => 'German',
            'code' => 'de',
        ],
        [
            'name' => 'Russian',
            'code' => 'ru',
        ],
        [
            'name' => 'French',
            'code' => 'fr',
        ],
        [
            'name' => 'Spanish',
            'code' => 'es',
        ],
        [
            'name' => 'Italian',
            'code' => 'it',
        ],
        [
            'name' => 'Portuguese',
            'code' => 'pt',
        ],
        [
            'name' => 'Chinese',
            'code' => 'cn',
        ],
        [
            'name' => 'Danish',
            'code' => 'dk',
        ]
      ]
    );
    }
}
