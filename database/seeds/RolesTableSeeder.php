<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('roles')->insert([
        [
            'name' => 'Admin',
            'seller' => 1,
            'admin' => 1,
            'register_user' => 1,
        ],
        [
            'name' => 'Seller',
            'seller' => 1,
            'admin' => 0,
            'register_user' => 1,
        ],
        [
            'name' => 'Register users',
            'seller' => 0,
            'admin' => 0,
            'register_user' => 1,
        ]
      ]
    );
    }
}
