<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('currencies')->insert([
        [
            'name' => 'USD',
            'code' => 'usd',
            'current_value' => 1,
        ],
        [
            'name' => 'EUR',
            'code' => 'eur',
            'current_value' => 1.2,
        ]
      ]);
    }
}
