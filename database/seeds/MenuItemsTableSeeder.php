<?php

use Illuminate\Database\Seeder;

class MenuItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Меню админки
        $adminMenu = \App\Menu::where('name', '=', 'Admin')->first();

        $adminItems = [];
        $adminItems[0] = array(
            'title' => 'Settings',
            'route_name' => 'admin.settings'
        );
        $adminItems[1] = array(
            'title' => 'Categories',
            'route_name' => 'admin.categories'
        );
        $adminItems[3] = array(
            'title' => 'Products',
            'route_name' => 'admin.products'
        );
        $adminItems[4] = array(
            'title' => 'Orders',
            'route_name' => 'admin.orders'
        );
        $adminItems[5] = array(
            'title' => 'News',
            'route_name' => 'admin.news'
        );
        $adminItems[6] = array(
            'title' => 'Users',
            'route_name' => 'admin.users'
        );
        $adminItems[7] = array(
            'title' => 'Sliders',
            'route_name' => 'admin.sliders'
        );
        $adminItems[8] = array(
            'title' => 'Banners',
            'route_name' => 'admin.banners'
        );
        $adminItems[9] = array(
            'title' => 'Currencies',
            'route_name' => 'admin.currencies'
        );
        $adminItems[10] = array(
            'title' => 'Static files',
            'route_name' => 'admin.test'
        );

        //Меню главное
        $mainMenu = \App\Menu::where('name', '=', 'Main')->first();

        $mainItems = [];
        $mainItems[0] = array(
            'title' => 'Main',
            'route_name' => 'home'
        );
        $mainItems[1] = array(
            'title' => 'My Orders',
            'route_name' => 'myOrders'
        );
        $mainItems[2] = array(
            'title' => 'New',
            'route_name' => 'news'
        );
        $mainItems[3] = array(
            'title' => 'About',
            'route_name' => 'about'
        );

        $infoMenu = \App\Menu::where('name', '=', 'Information')->first();
        $infoItems = [];
        $infoItems[0] = array(
            'title' => 'SPECIALS',
            'route_name' => ''
        );
        $infoItems[1] = array(
            'title' => 'NEW PRODUCTS',
            'route_name' => ''
        );
        $infoItems[2] = array(
            'title' => 'SPECIALS',
            'route_name' => ''
        );
        $infoItems[3] = array(
            'title' => 'TOP SELLERS',
            'route_name' => ''
        );
        $infoItems[4] = array(
            'title' => 'OUR STORES',
            'route_name' => ''
        );
        $infoItems[5] = array(
            'title' => 'CONTACT US',
            'route_name' => ''
        );
        $infoItems[6] = array(
            'title' => 'DELIVERY',
            'route_name' => ''
        );
        $infoItems[7] = array(
            'title' => 'ABOUT US',
            'route_name' => ''
        );
        $infoItems[8] = array(
            'title' => 'SECURE PAYMENT',
            'route_name' => ''
        );
        $infoItems[9] = array(
            'title' => 'PAGES CONFIGURATION',
            'route_name' => ''
        );
        $infoItems[10] = array(
            'title' => 'WARRANTY',
            'route_name' => ''
        );
        $infoItems[11] = array(
            'title' => 'FAQS',
            'route_name' => ''
        );

        foreach ($adminItems as $adminItem) {
            $menuItem = new \App\MenuItem();
            $menuItem->menu_id = $adminMenu->id;
            $menuItem->title = $adminItem['title'];
            $menuItem->route_name = $adminItem['route_name'];
            $menuItem->save();
        }


        foreach ($mainItems as $mainItem) {
            $menuItem = new \App\MenuItem();
            $menuItem->menu_id = $mainMenu->id;
            $menuItem->title = $mainItem['title'];
            $menuItem->route_name = $mainItem['route_name'];
            $menuItem->save();
        }

        foreach ($infoItems as $infoItem) {
            $infoItem = new \App\MenuItem();
            $infoItem->menu_id = $infoMenu->id;
            $infoItem->title = $infoItem['title'];
            $infoItem->route_name = $infoItem['route_name'];
            $infoItem->save();
        }
    }
}
