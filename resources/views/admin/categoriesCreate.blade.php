@extends('admin.base')

@section('main-content')
    <div class="row mb-3">
        <h2 class="col-lg-12">New categorie</h2>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form method="POST"  action="{{route('admin.categories.create_post')}}">
                {{csrf_field()}}
                <div class="form-group" style="min-height: 1.5rem; color: red">
                    {{$message}}
                </div>
                <div class="form-group">
                    <label for="categories">Language</label>
                    <select class="form-control" name="lang_id" id="lang_id">
                        @foreach($langs as $lang)
                            <option value="{{$lang->code}}" @if(isset(Request::all()['lang_id']) && Request::all()['lang_id'] == $lang->code) selected @endif>{{$lang->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="categories">Categorie</label>
                    <select class="form-control" name="parent_id" id="categories">
                        <option value="0">Choose section</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}" @if(isset(Request::all()['categorie_id']) && Request::all()['categorie_id'] == $category->id) selected @endif>{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="inputTitle">Name</label>
                    <input type="text" name="title" placeholder="Name" class="form-control" id="inputTitle" value="{{isset(Request::all()['name']) ? Request::all()['name'] : ''}}">
                </div>
                <div class="form-group">
                    <label for="inputDesc">Description</label>
                    <textarea name="description" id="inputDesc" placeholder="Описание" class="form-control" rows="5">{{isset(Request::all()['description']) ? Request::all()['description'] : ''}}</textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{route('admin.categories')}}" class="btn btn-primary ml-3">Back</a>
                </div>
            </form>
        </div>
    </div>
@endsection
