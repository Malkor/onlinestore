@extends('admin.base')

@section('main-content')
    <div class="row mb-3">
        <h2 class="col-lg-12">Edit post</h2>
    </div>
    <div class="row mb-3">
        <div class="col-lg-12" style="min-height: 1.5rem; color: red">
            {{$message}}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form id="postsForm" enctype="multipart/form-data" method="POST"  action="{{route('admin.posts.edit_post', ['post_id' => $post->id])}}">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="inputTitle">Title</label>
                    <input type="text" name="title" placeholder="Title" class="form-control" id="inputTitle" value="{{$post->title}}">
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
              <label for="categories">Language</label>
              <select class="form-control" form="postsForm" name="lang_id" id="categories" value = ''>
                  @foreach($langs as $lang)
                      <option value="{{$lang->code}}" @if($lang->code == $post->lang_id) selected @endif>{{$lang->name}}</option>
                  @endforeach
              </select>
          </div>
            <div class="form-group">
                <label for="inputText">Text</label>
                <textarea form="postsForm" name="description" id="inputText" placeholder="Text" class="form-control" rows="10">{{$post->description}}</textarea>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="mb-3" style="height: 150px; background-image: url('{{$post->pic}}'); background-size: cover;">
                <img src="" alt="картинка новости" style="display: none;">
            </div>
            <label for="file">Image</label>
            <input form="postsForm" name="pic" type="file" id="file" class="form-control-file mb-3">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div class="form-group">
                <label for="inputExcerpt">Excerpt</label>
                <textarea form="postsForm" name="excerpt" id="inputExcerpt" placeholder="Отрывок" class="form-control" rows="5">{{$post->excerpt}}</textarea>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="form-group">
                <label for="inputTags">Tags</label>
                <input form="postsForm" type="text" name="tags" placeholder="Example(some, tags, for_us)" class="form-control" id="inputTags" value="{{$post->tags}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <button form="postsForm" type="submit" class="btn btn-primary">Save</button>
            <a href="{{route('admin.posts')}}" class="btn btn-primary ml-3">Back</a>
            <a href="{{route('admin.posts.delete', ['post_id' => $post->id])}}" class="btn btn-danger ml-3">Delete</a>
        </div>
    </div>
@endsection
