@extends('admin.base')

@section('main-content')
    <div class="row mb-3">
        <h2 class="col-lg-12">Currencies</h2>
    </div>
    <div class="row mb-3">
        <div class="col-lg-12">
            <a class="btn btn-primary" href="{{route('admin.currencies.create')}}" role="button">Add</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-hover table-bordered">
                <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Current value</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($currencies as $cur)
                    <tr id="{{$cur->id}}">
                        <td>{{$cur->id}}</td>
                        <td>{{$cur->name}}</td>
                        <td>{{$cur->current_value}}</td>
                        <td><a class="badge badge-primary" href="{{route('admin.currencies.edit' , ['cur_id' => $cur->id])}}">change</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
