@extends('admin.base')

@section('main-content')
    <div class="row mb-3">
        <h2 class="col-lg-12">Sliders</h2>
    </div>
    <div class="row mb-3">
        <div class="col-lg-12">
            <a class="btn btn-primary" href="{{route('admin.sliders.create')}}" role="button">Add</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-hover table-bordered">
                <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Link</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sliders as $slider)
                    <tr id="{{$slider->id}}">
                        <td>{{$slider->id}}</td>
                        <td>{{$slider->name}}</td>
                        <td>{{$slider->link}}</td>
                        <td><a class="badge badge-primary" href="{{route('admin.sliders.edit' , ['slider_id' => $slider->id])}}">change</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
