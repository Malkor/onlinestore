@extends('admin.base')

@section('main-content')
    <div class="row mb-3">
        <h2 class="col-lg-12">Users</h2>
    </div>
    <!--div class="row mb-3">
        <div class="col-lg-12">
            <a href="{{route('admin.news.create')}}" class="btn btn-primary">Add</a>
        </div>
    </div-->
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-hover table-bordered">
                <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>E-Mail</th>
                    <th>Group</th>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr id="{{$user->id}}">
                        <td>{{$user->id}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->roles}}</td>
                        <td>{{$user->name}}</td>
                        <td>
                            <a class="badge badge-primary" href="{{route('admin.users.edit' , ['user_id' => $user->id])}}">Edit</a>
                            <a class="badge badge-primary" href="{{route('admin.users.delete' , ['user_id' => $user->id])}}">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
