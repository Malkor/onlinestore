@extends('admin.base')

@section('main-content')
    <div class="row mb-3">
        <h2 class="col-lg-12">Edit post</h2>
    </div>
    <div class="row mb-3">
        <div class="col-lg-12" style="min-height: 1.5rem; color: red">
            {{$message}}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form id="postsForm" enctype="multipart/form-data" method="POST"  action="{{route('admin.menus.edit_post', ['menu_id' => $menu->id])}}">
                {{csrf_field()}}
                <label for="active">Active</label>
                <input type="checkbox" name="active" placeholder="Name" class="form-control" @if($menu->active == 1) checked @endif>
                <div class="form-group">
                    <label for="inputTitle">Title</label>
                    <input type="text" name="name" placeholder="Title" class="form-control" id="inputTitle" value="{{$menu->name}}">
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
              <label for="categories">Language</label>
              <select class="form-control" form="postsForm" name="lang_id" id="categories" value = ''>
                  @foreach($langs as $lang)
                      <option value="{{$lang->code}}" @if($lang->code == $menu->lang_id) selected @endif>{{$lang->name}}</option>
                  @endforeach
              </select>
          </div>
          <div class="form-group">
              <label for="categories">Choose type menu</label>
              <select class="form-control" form="postsForm" name="type_menu" id="type_menu">
                      <option value="Header">Header</option>
                      <option value="Footer">Footer</option>
              </select>
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
              <label for="inputTitle">Route(path)</label>
              <input type="text" form="postsForm" name="route" placeholder="Title" class="form-control" id="inputTitle" value="{{$menu->route}}">
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <button form="postsForm" type="submit" class="btn btn-primary">Save</button>
            <a href="{{route('admin.menus')}}" class="btn btn-primary ml-3">Back</a>

        </div>
    </div>
@endsection
