@extends('admin.base')

@section('main-content')
    <div class="row mb-3">
        <h2 class="col-lg-12">Change product "{{$product->name}}"</h2>
    </div>
    <div class="row mb-3">
        <div class="col-lg-12" style="min-height: 1.5rem; color: red">
            {{$message}}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <form id="productForm" enctype="multipart/form-data" method="POST"  action="{{route('admin.products.edit_post', ['product_id' => $product->id])}}">
                {{csrf_field()}}
                <label for="active">Active</label>
                <input type="checkbox" name="active" placeholder="Name" class="form-control" @if($product->active == 1) checked @endif>
                <div class="form-group">
                    <label for="inputTitle">Name</label>
                    <input type="text" name="name" placeholder="Name" class="form-control" id="inputTitle" value="{{$product->name}}">
                </div>
            </form>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label for="categories">Categorie</label>
                <select class="form-control" form="productForm" name="categorie_id" id="categories" value = ''>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}" @if($category->id == $product->categorie_id) selected @endif>{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label for="categories">Language</label>
                <select class="form-control" form="productForm" name="lang_id" id="categories" value = ''>
                    @foreach($langs as $lang)
                        <option value="{{$lang->code}}" @if($lang->code == $product->lang_id) selected @endif>{{$lang->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div class="form-group">
                <label for="inputDesc">Description</label>
                <textarea form="productForm" name="description" id="inputDesc" placeholder="Description" class="form-control" rows="10">{{$product->description}}</textarea>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="product__thumbnail mb-3" style="height: 150px; background-image: url('{{$product->pic}}'); -webkit-background-size: cover;background-size: cover;">
                <img src="" alt="картинка товара" style="display: none;">
            </div>
            <label for="file">Image</label>
            <input form="productForm" name="pic" type="file" id="file" class="form-control-file mb-3">
            <label for="price">Price</label>
            <input type="text" form="productForm" class="form-control" name="price" id="price" value="{{$product->price}}">
            <label for="old_price">Old price</label>
            <input type="text" form="productForm" class="form-control" name="old_price" id="old_price" value="{{$product->old_price}}">
            <hr>
              <label for="top">Top element</label>
              <input type="checkbox" form="productForm" class="form-control" name="top" id="top" @if($product->top == 1) checked @endif >
              <label for="new">New element</label>
              <input type="checkbox" form="productForm" class="form-control" name="new" id="new" @if($product->new == 1) checked @endif >
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <button form="productForm" type="submit" class="btn btn-primary">Save</button>
            <a href="{{route('admin.products')}}" class="btn btn-primary ml-3">Back</a>
            <a href="{{route('admin.products.delete', ['product_id' => $product->id])}}" class="btn btn-danger ml-3">Delete</a>
        </div>
    </div>
@endsection
