@extends('admin.base')

@section('main-content')
    <div class="row mb-3">
        <h2 class="col-lg-12">Edit currency</h2>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form method="POST" enctype="multipart/form-data" action="{{route('admin.currencies.edit_post', ['currency_id' => $currency->id])}}">
                {{csrf_field()}}
                <div class="form-group" style="min-height: 1.5rem; color: red">
                    {{$message}}
                </div>
                <div class="form-group">
                    <label for="inputTitle">Name</label>
                    <input type="text" name="name" placeholder="Name" class="form-control" id="inputTitle" value="{{$currency->name}}">
                </div>
                <div class="form-group">
                    <label for="inputTitle">Code</label>
                    <input type="text" disabled name="code" placeholder="Code" class="form-control" id="inputTitle" value="{{$currency->code}}">
                </div>
                <div class="form-group">
                    <label for="inputTitle">Edit value</label>
                    <input type="text" name="current_value" placeholder="Link" class="form-control" id="inputTitle" value="{{$currency->current_value}}">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{route('admin.currencies')}}" class="btn btn-primary ml-3">Back</a>

                </div>
            </form>
        </div>
    </div>
@endsection
