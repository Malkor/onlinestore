@extends('admin.base')

@section('main-content')
    <div class="row mb-3">
        <h2 class="col-lg-12">Edit slider</h2>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form method="POST" enctype="multipart/form-data" action="{{route('admin.sliders.edit_post', ['slider_id' => $slider->id])}}">
                {{csrf_field()}}
                <div class="form-group" style="min-height: 1.5rem; color: red">
                    {{$message}}
                </div>
                <div class="form-group">
                    <label for="inputTitle">Name</label>
                    <input type="text" name="name" placeholder="Name" class="form-control" id="inputTitle" value="{{$slider->name}}">
                </div>
                <div class="form-group">
                    <label for="inputTitle">Enter link</label>
                    <input type="text" name="link" placeholder="Link" class="form-control" id="inputTitle" value="{{$slider->link}}">
                </div>
                <div class="form-group">
                  <div class="product__thumbnail mb-3" style="height: 150px; background-image: url('{{$slider->pic}}'); -webkit-background-size: cover;background-size: cover;">
                      <img src="" alt="картинка товара" style="display: none;">
                  </div>
                  <label for="file">Image</label>
                  <input  name="pic" type="file" id="file" class="form-control-file mb-3">
                </div>
                <div class="form-group">
                    <label for="inputDesc">Description</label>
                    <textarea  name="text" id="inputDesc" placeholder="" class="form-control" rows="10">{{$slider->text}}</textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{route('admin.sliders')}}" class="btn btn-primary ml-3">Back</a>
                    <a href="{{route('admin.sliders.delete', ['slider_id' => $slider->id])}}" class="btn btn-danger ml-3">Delete</a>
                </div>
            </form>
        </div>
    </div>
@endsection
