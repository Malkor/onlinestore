@extends('admin.base')

@section('main-content')
    <div class="row mb-3">
        <h2 class="col-lg-12">Add post</h2>
    </div>
    <div class="row mb-3">
        <div class="col-lg-12" style="min-height: 1.5rem; color: red">
            {{$message}}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form id="postsForm" enctype="multipart/form-data" method="POST"  action="{{route('admin.menus.create_post')}}">
                {{csrf_field()}}
                <label for="active">Active</label>
                <input type="checkbox" name="active" placeholder="Name" class="form-control" value="1">
                <div class="form-group">
                    <label for="inputTitle">Title</label>
                    <input type="text" name="name" placeholder="Title" class="form-control" id="inputTitle" value="{{isset(Request::all()['name']) ? Request::all()['name'] : ''}}">
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
              <label for="categories">Language</label>
              <select class="form-control" form="postsForm" name="lang_id" id="lang_id">
                  @foreach($langs as $lang)
                      <option value="{{$lang->code}}" @if(isset(Request::all()['lang_id']) && Request::all()['lang_id'] == $lang->code) selected @endif>{{$lang->name}}</option>
                  @endforeach
              </select>
          </div>
          <div class="form-group">
              <label for="categories">Choose type menu</label>
              <select class="form-control" form="postsForm" name="type_menu" id="lang_id">
                      <option value="Header">Header</option>
                      <option value="Footer">Footer</option>
              </select>
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
              <label for="inputTitle">Route(path)</label>
              <input type="text" form="postsForm" name="route" placeholder="Title" class="form-control" id="inputTitle" value="{{isset(Request::all()['route']) ? Request::all()['route'] : ''}}">
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <button form="postsForm" type="submit" class="btn btn-primary">Save</button>
            <a href="{{route('admin.posts')}}" class="btn btn-primary ml-3">Back</a>
        </div>
    </div>
@endsection
