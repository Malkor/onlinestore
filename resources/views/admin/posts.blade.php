@extends('admin.base')

@section('main-content')
    <div class="row mb-3">
        <h2 class="col-lg-12">Blog posts</h2>
    </div>
    <div class="row mb-3">
        <div class="col-lg-12">
            <a href="{{route('admin.posts.create')}}" class="btn btn-primary">Add</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-hover table-bordered">
                <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Excerpt</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)
                    <tr id="{{$post->id}}">
                        <td>{{$post->id}}</td>
                        <td>{{$post->title}}</td>
                        <td>{{$post->excerpt}}</td>
                        <td><a class="badge badge-primary" href="{{route('admin.posts.edit' , ['post_id' => $post->id])}}">Edit</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
