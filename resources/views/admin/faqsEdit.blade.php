@extends('admin.base')

@section('main-content')
    <div class="row mb-3">
        <h2 class="col-lg-12">Add FAQ</h2>
    </div>
    <div class="row mb-3">
        <div class="col-lg-12" style="min-height: 1.5rem; color: red">
            {{$message}}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form id="newsForm" enctype="multipart/form-data" method="POST"  action="{{route('admin.faqs.edit_post', ['faq_id' => $faq->id])}}">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="inputTitle">Active</label>
                    <input type="checkbox" name="active" placeholder="Name" class="form-control" @if($faq->active == 1) checked @endif>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="categories">Language</label>
                        <select class="form-control" name="lang_id" id="categories">
                            @foreach($langs as $lang)
                              <option value="{{$lang->code}}" @if($lang->code == $faq->lang_id) selected @endif>{{$lang->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputTitle">Question</label>
                    <input type="text" name="question" placeholder="Question" class="form-control" id="inputTitle" value="{{$faq->question}}">
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div class="form-group">
                <label for="inputText">Answer</label>
                <textarea form="newsForm" name="answer" id="inputText" placeholder="Answer" class="form-control" rows="10">{{$faq->answer}}</textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <button form="newsForm" type="submit" class="btn btn-primary">Save</button>
            <a href="{{route('admin.faqs')}}" class="btn btn-primary ml-3">Back</a>
        </div>
    </div>
@endsection
