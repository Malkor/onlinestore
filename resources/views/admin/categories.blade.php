@extends('admin.base')

@section('main-content')
    <div class="row mb-3">
        <h2 class="col-lg-12">Categories</h2>
    </div>
    <div class="row mb-3">
        <div class="col-lg-12">
            <a class="btn btn-primary" href="{{route('admin.categories.create')}}" role="button">Add</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-hover table-bordered">
                <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $categorie)
                    <tr id="{{$categorie->id}}">
                        <td>{{$categorie->id}}</td>
                        <td>{{$categorie->title}}</td>
                        <td>{{$categorie->description}}</td>
                        <td><a class="badge badge-primary" href="{{route('admin.categories.edit' , ['categorie_id' => $categorie->id])}}">change</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
