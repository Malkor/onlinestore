@extends('admin.base')

@section('main-content')
    <div class="row mb-3">
        <h2 class="col-lg-12">New banner</h2>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form method="POST" enctype="multipart/form-data" action="{{route('admin.banners.create_post')}}">
                {{csrf_field()}}
                <div class="form-group" style="min-height: 1.5rem; color: red">
                    {{$message}}
                </div>
                <div class="form-group">
                    <label for="categories">Which category show banner?</label>
                    <select class="form-control" name="categorie_id" id="categories" value = ''>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}" >{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="inputTitle">Name</label>
                    <input type="text" name="name" placeholder="Name" class="form-control" id="inputTitle" value="{{isset(Request::all()['name']) ? Request::all()['name'] : ''}}">
                </div>
                <div class="form-group">
                    <label for="inputTitle">Enter link</label>
                    <input type="text" name="link" placeholder="Link" class="form-control" id="inputTitle" value="{{isset(Request::all()['link']) ? Request::all()['link'] : ''}}">
                </div>
                <div class="form-group">
                  <label for="file">Image</label>
                  <input  name="pic" type="file" id="file" class="form-control-file mb-3">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{route('admin.banners')}}" class="btn btn-primary ml-3">Back</a>
                </div>
            </form>
        </div>
    </div>
@endsection
