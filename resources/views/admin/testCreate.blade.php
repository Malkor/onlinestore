@extends('admin.base')

@section('main-content')
    <div class="row mb-3">
        <h2 class="col-lg-12">Edit static files</h2>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form method="POST" enctype="multipart/form-data" action="{{route('admin.test.edit_post', ['condition' => $condition])}}">
                {{csrf_field()}}
                <div class="form-group">
                  <label for="test">About</label>
                </div>
                <select class="" name="lang_id">
                  @foreach($langs as $lang)
                    <option value="{{$lang->code}}">{{$lang->name}}</option>
                  @endforeach
                </select>
                <div class="form-group">
                    <textarea name="text" id="inputTitle" rows="8" cols="80">{{ $data->text }}</textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{route('admin.test')}}" class="btn btn-primary ml-3">Back</a>
                </div>
            </form>
        </div>
    </div>
@endsection
