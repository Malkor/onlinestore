@extends('admin.base')

@section('main-content')
    <div class="row mb-3">
        <h2 class="col-lg-12">Banners</h2>
    </div>
    <div class="row mb-3">
        <div class="col-lg-12">
            <a class="btn btn-primary" href="{{route('admin.banners.create')}}" role="button">Add</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-hover table-bordered">
                <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Link</th>
                    <th>Category</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($banners as $banner)
                    <tr id="{{$banner->id}}">
                        <td>{{$banner->id}}</td>
                        <td>{{$banner->name}}</td>
                        <td>{{$banner->link}}</td>
                        <td>{{$banner->categorie_id}}</td>
                        <td><a class="badge badge-primary" href="{{route('admin.banners.edit' , ['banner_id' => $banner->id])}}">change</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
