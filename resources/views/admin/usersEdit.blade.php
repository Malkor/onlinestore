@extends('admin.base')

@section('main-content')
    <div class="row mb-3">
        <h2 class="col-lg-12">Edit user</h2>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form method="POST" enctype="multipart/form-data" action="{{route('admin.users.edit_post', ['user_id' => $user->id])}}">
                {{csrf_field()}}
                <div class="form-group" style="min-height: 1.5rem; color: red">
                    {{$message}}
                </div>
                <div class="form-group">
                    <label for="inputTitle">First name</label>
                    <input type="text" name="first_name" placeholder="First name" class="form-control" id="inputTitle" value="{{$user->first_name}}">
                </div>
                <div class="form-group">
                    <label for="inputTitle">Name</label>
                    <input type="text" name="name" placeholder="Name" class="form-control" id="inputTitle" value="{{$user->name}}">
                </div>
                <div class="form-group">
                    <label for="inputTitle">Last name</label>
                    <input type="text" name="last_name" placeholder="Last name" class="form-control" id="inputTitle" value="{{$user->last_name}}">
                </div>
                <div class="form-group">
                    <label for="inputTitle">E-Mail</label>
                    <input disabled type="text" name="email" placeholder="E-Mail" class="form-control" id="inputTitle" value="{{$user->email}}">
                </div>
                <div class="form-group">
                    <label for="roleData">User roles</label><br>
                    @foreach($role_info as $role)
                      {{$role->name}}
                      <input type="radio" id="roleData" name="role" @if($current_role->id == $role->id) checked @endif value="{{$role->id}}"><br>
                    @endforeach
                </div>
                <div class="form-group">
                    <label for="inputTitle">Phone</label>
                    <input type="text" name="phone" placeholder="Phone" class="form-control" id="inputTitle" value="{{$user->phone}}">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{route('admin.users')}}" class="btn btn-primary ml-3">Back</a>
                    <a href="{{route('admin.users.delete', ['user_id' => $user->id])}}" class="btn btn-danger ml-3">Delete</a>
                </div>
            </form>
        </div>
    </div>
@endsection
