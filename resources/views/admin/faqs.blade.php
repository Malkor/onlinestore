@extends('admin.base')

@section('main-content')
    <div class="row mb-3">
        <h2 class="col-lg-12">FAQs</h2>
    </div>
    <div class="row mb-3">
        <div class="col-lg-12">
            <a href="{{route('admin.faqs.create')}}" class="btn btn-primary">Add</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-hover table-bordered">
                <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Question</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($faqs as $faq)
                    <tr id="{{$faq->id}}">
                        <td>{{$faq->id}}</td>
                        <td>{{$faq->question}}</td>
                        <td><a class="badge badge-primary" href="{{route('admin.faqs.edit' , ['faq_id' => $faq->id])}}">Edit</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
