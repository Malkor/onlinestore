<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static findOrFail($get)
 * @method static where(string $string, $id)
 */
class PaymentCard extends Model
{
  protected $fillable = [
    'user_id',
    'owner',
    'card_number'
  ];
}
