<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{

  protected $fillable = ['name', 'code', 'current_value'];

  public function product()
  {
      return $this->hasMany('App\Product');
  }

}
