<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
  protected $fillable = [
      'name', 'seller', 'admin', 'register_user'
  ];

  public function userData()
  {
      return $this->belongsTo('App\User');
  }

}
