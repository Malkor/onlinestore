<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\RegisterAuthRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class ApiController extends Controller
{
    public $loginAfterSignUp = true;

    //public function register(RegisterAuthRequest $request)
    public function register(Request $request)
    {

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->phone = $request->phone;
        $user->default_currency = '1';
        $user->role = 3;
        $user->lang_id = "en";
        $user->password = bcrypt($request->password);
        $user->save();

        if ($this->loginAfterSignUp) {
            return $this->login($request);
        }

        return response()->json([
            'success' => true,
            'data' => $user,
        ], 200);
    }

    public function login(Request $request)
    {
        $input = $request->only('email', 'password');
        $jwt_token = null;

        if (!$jwt_token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], 401);
        }

        return response()->json([
            'success' => true,
            'token' => $jwt_token,
        ]);
    }

    public function logout(Request $request)
    {
      dd($request);
        try {
            JWTAuth::invalidate($request->token);

            return response()->json([
                'success' => true,
                'message' => 'User logged out successfully'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, the user cannot be logged out',
                'add' => $request->token
            ], 500);
        }
    }

    public function getAuthUser(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        return response()->json(['user' => $user], 200);
    }

    public function setLang(Request $request)
    {

      $user = JWTAuth::parseToken()->authenticate();
      $user->lang_id = $request->lang_id;
      $user->save();
      return response()->json(['user' => $user], 200);
    }


    public function setUserRole(Request $request)
    {
      $user = JWTAuth::parseToken()->authenticate();
      $user->role_id = 2;
      $user->save();
      return response()->json(['user' => $user], 200);

    }


}
