<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

class UserList extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $users = User::all();
      #dd($users);
      return view('admin.users', ['users' => $users]);
    }


    public function edit($user_id, Request $request)
    {
        $user = User::find($user_id);
        $roleInfo = Role::all();
        $currentRole = Role::where('id', '=', $user->role)->first();
        #dd($roleInfo);
        $message ='';
        if (Route::currentRouteName() == 'admin.users.edit_post') {
            $rules = array(
                'name' => 'required',
            );

            $messages = array(
                'name.required' => 'Имя должно быть заполнено',
            );

            $validation = Validator::make($request->all(), $rules, $messages);
            if ($validation->fails()) {
                $message = $validation->errors()->first();
                return view('admin.usersEdit', [
                    'user' => $user,
                    'message' => $message
                    ]);
            } else {
              #dd($request->all());
                $user->name = $request->all()['name'];
                $user->first_name = $request->all()['first_name'];
                $user->last_name = $request->all()['last_name'];
                $user->phone = $request->all()['phone'];
                $user->role = $request->all()['role'];
                $user->save();
                return redirect()->route('admin.users');
            }
        }
        return view('admin.usersEdit', ['user' => $user, 'role_info' => $roleInfo, 'current_role' => $currentRole, 'message' => $message]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($user_id)
    {
      $users = User::find($user_id);
      $users->delete();

      return redirect()->route('admin.users');
    }
}
