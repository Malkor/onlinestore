<?php

namespace App\Http\Controllers\Admin;

use App\Currency;

class UpdateCurrency
{
  public function saveCurrenciesValue()
  {
    $new_currency_value = $this->getCurrencyCourse(); // Список валют со значениями полученными по API
    $currencies_list = Currency::all(); // Наш список валют
    foreach ($currencies_list as $item) {
      try {
        if (mb_strtoupper($item->code) === "USD") {
          // USD всегда 1
          $item->current_value = 1;
        }else{
          $current_value = $new_currency_value[mb_strtoupper($item->code)];
          $percent = $current_value / 100 * 10;
          $item->current_value = $current_value + $percent;
        }

        $item->save();

      } catch (\Exception $e) {
        continue;
      }
    }
  }

  // Создаст список вех валют !
//  public function newCurr()
//  {
//    $new_currency_value = $this->getCurrencyCourse();
//    foreach (array_keys($new_currency_value) as $item){
//      $el = new UserCurrency();
//      $el->name = $item;
//      $el->code = $item;
//      $el->current_value = 1;
//
//      $el->save();
//    }
//
//    return array_keys($new_currency_value);
//  }

  private function getCurrencyCourse()
  {
    $ch = curl_init('https://api.exchangeratesapi.io/latest?base=USD');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    $response = (json_decode(curl_exec($ch), true))['rates'];
    curl_close($ch);

    return $response;
  }
}
