<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

use App\Currency;


class CurrencyList extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currency = Currency::all();
        return view('admin.currencies', ['currencies' => $currency]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $message ='';

      if (Route::currentRouteName() == 'admin.currencies.create_post') {
          $rules = array(
              'name' => 'required',
              'code' => 'required',
              'current_value' => 'required',
          );

          $messages = array(
              'name.required' => 'Enter name',
              'code.required' => 'Code your currency',
              'current_value.required' => 'Default value',
          );

          $validation = Validator::make($request->all(), $rules, $messages);

          if ($validation->fails()) {
              $message = $validation->errors()->first();
              return view('admin.currenciesCreate', ['message' => $message]);
          } else {
              $currency = new Currency();
              $currency->name = $request->all()['name'];
              $currency->code = $request->all()['code'];
              $currency->current_value = $request->all()['current_value'];
              $currency->save();

              return redirect()->route('admin.currencies');
          }
      }
      return view('admin.currenciesCreate', ['message' => $message]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($currency_id, Request $request)
    {
      $currency = Currency::find($currency_id);

      $message ='';
      if (Route::currentRouteName() == 'admin.currencies.edit_post') {
          $rules = array(
            'name' => 'required'
          );

          $messages = array(
            'name.required' => 'Название должно быть заполнено'
          );

          $validation = Validator::make($request->all(), $rules, $messages);
          #dd($request->new);
          if ($validation->fails()) {
              $message = $validation->errors()->first();
              return view('admin.currenciesEdit', [
                  'currency' => $currency,
                  'message' => $message
                  ]);
          } else {
              $currency->name = $request->all()['name'];
              //$currency->code = $request->all()['code'];
              $currency->current_value = $request->all()['current_value'];
              $currency->save();
              return redirect()->route('admin.currencies');
          }
      }

      return view('admin.currenciesEdit', ['currency' => $currency, 'message' => $message]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
