<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PublicMenu;
use App\Language;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Route;

class PublicMenuController extends Controller
{
    public function index()
    {
      $getMenu = PublicMenu::all();
      return view('admin.menus', ['menus' => $getMenu]);
    }

    public function create(Request $request)
    {
        $langs = Language::all();
        $message ='';
        if (Route::currentRouteName() == 'admin.menus.create_post') {
            $rules = array(
                'name' => 'required',
            );

            $messages = array(
                'name.required' => 'Название должно быть заполнено',
            );

            $validation = Validator::make($request->all(), $rules, $messages);
            if ($validation->fails()) {
                $message = $validation->errors()->first();
                return view('admin.menusCreate', ['message' => $message, 'categories' => $categories, 'langs' => $langs]);
            } else {
                #dd($request->all());
                $menu = new PublicMenu();
                if(!empty($request->all()['active'])){
                  if($request->all()['active'] == 1){
                    $menu->active = true;
                  }
                }else{
                  $menu->active = false;
                }
                $menu->name = $request->all()['name'];
                $menu->route = $request->all()['route'];
                $menu->lang_id = $request->all()['lang_id'];
                $menu->type_menu = $request->all()['type_menu'];
                $menu->save();
                return redirect()->route('admin.menus');
            }
        }

        return view('admin.menusCreate', ['message' => $message, 'langs' => $langs]);
    }


    public function edit($menu_id, Request $request)
    {
        $menu = PublicMenu::find($menu_id);
        $langs = Language::all();
        $message ='';
        if (Route::currentRouteName() == 'admin.menus.edit_post') {
            $rules = array(
                'name' => 'required',
            );

            $messages = array(
                'name.required' => 'Название должно быть заполнено',
            );

            $validation = Validator::make($request->all(), $rules, $messages);
            #dd($request->new);
            if ($validation->fails()) {
                $message = $validation->errors()->first();
                return view('admin.menusEdit', [
                    'menu' => $menu,
                    'langs' => $langs,
                    'message' => $message
                    ]);
            } else {
              #dd($request->all());
                if(!empty($request->all()['active'])){
                  if($request->active == 'on'){
                      $menu->active = 1;
                  }
                }else{
                  $menu->active = false;
                }
                $menu->name = $request->all()['name'];
                $menu->route = $request->all()['route'];
                $menu->lang_id = $request->all()['lang_id'];
                $menu->type_menu = $request->all()['type_menu'];
                $menu->lang_id = $request->all()['lang_id'];
                $menu->save();
                return redirect()->route('admin.menus');
            }
        }

        return view('admin.menusEdit', ['menu' => $menu, 'message' => $message, 'langs' => $langs]);
    }
}
