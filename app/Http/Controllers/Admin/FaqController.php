<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Faq;
use App\Language;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

class FaqController extends Controller
{
    public function list()
    {
      $faqList = Faq::all();
      return view('admin.faqs', ['faqs' => $faqList]);
    }

    public function create(Request $request)
    {
        $langs = Language::all();
        $message ='';
        if (Route::currentRouteName() == 'admin.faqs.create_post') {
            $rules = array(
                'question' => 'required',
            );

            $messages = array(
                'question.required' => 'Вопрос должен быть заполнен',
            );

            $validation = Validator::make($request->all(), $rules, $messages);

            if ($validation->fails()) {
                $message = $validation->errors()->first();
                return view('admin.faqsCreate', ['message' => $message, 'langs' => $langs]);
            } else {
            #dd($request->all());
                $faq = new Faq();
                if(!empty($request->all()['active'])){
                  if($request->all()['active'] == 1){
                    $faq->active = true;
                  }
                }else{
                  $faq->active = false;
                }
                $faq->question = $request->all()['question'];
                $faq->answer = $request->all()['answer'];
                $faq->lang_id = $request->all()['lang_id'];
                $faq->save();

                return redirect()->route('admin.faqs');
            }
        }
        return view('admin.faqsCreate', ['message' => $message, 'langs' => $langs]);
    }


    public function edit($faq_id, Request $request)
    {
      $langs = Language::all();
      $faq = Faq::find($faq_id);
      $message ='';
      if (Route::currentRouteName() == 'admin.faqs.edit_post') {
          $rules = array(
              'question' => 'required',
          );

          $messages = array(
              'question.required' => 'Название должно быть заполнено',
          );

          $validation = Validator::make($request->all(), $rules, $messages);
          if ($validation->fails()) {
              $message = $validation->errors()->first();
              return view('admin.faqsEdit', [
                  'faq' => $faq,
                  'message' => $message,
                  'langs' => $langs
                  ]);
          } else {
              if(!empty($request->all()['active'])){
                if($request->active == 'on'){
                    $faq->active = 1;
                }
              }else{
                $faq->active = false;
              }
              $faq->question = $request->all()['question'];
              $faq->answer = $request->all()['answer'];
              $faq->lang_id = $request->all()['lang_id'];

              $faq->save();
              return redirect()->route('admin.faqs');
          }
      }

      return view('admin.faqsEdit', ['faq' => $faq, 'message' => $message, 'langs' => $langs]);
    }
}
