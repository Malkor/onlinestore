<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use App\Slider;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::all();
        return view('admin.sliders', ['sliders' => $sliders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
          $message ='';
          if (Route::currentRouteName() == 'admin.sliders.create_post') {
              $rules = array(
                  'name' => 'required',
                  'link' => 'required',
                  'pic' => 'required|image'
              );

              $messages = array(
                  'name.required' => 'Название должно быть заполнено',
                  'link.required' => 'Описание должно быть заполнено',
                  'pic.required' => 'Следует загрузить картинку',
                  'pic.image' => 'Выбранный файл не является изображением'
              );

              $validation = Validator::make($request->all(), $rules, $messages);
              //dd($request->file('pic'));
              if ($validation->fails()) {
                  $message = $validation->errors()->first();
                  return view('admin.slidersCreate', ['message' => $message]);
              } else {
                  $slider = new Slider();
                  $slider->name = $request->all()['name'];
                  $slider->link = $request->all()['link'];
                  $slider->text = $request->all()['text'];
                  $slider->save();

                  Storage::makeDirectory('/uploads/sliders/slider-id-' . $slider->id);

                  $request->file('pic')
                      ->move(storage_path() . '/app/public/uploads/sliders/slider-id-' . $slider->id, 'sliderPic.jpg');

                  $slider->pic = '/storage/uploads/sliders/slider-id-' . $slider->id . '/sliderPic.jpg';
                  $slider->save();
                  return redirect()->route('admin.sliders');
              }
          }

          return view('admin.slidersCreate', ['message' => $message]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slider_id, Request $request)
    {
      $slider = Slider::find($slider_id);
      $message ='';
      #dd(Route::currentRouteName());
      if (Route::currentRouteName() == 'admin.sliders.edit_post') {
          $rules = array(
              'name' => 'required',
              'link' => 'required',
              'pic' => 'image'
          );

          $messages = array(
              'name.required' => 'Название должно быть заполнено',
              'link.required' => 'Описание должно быть заполнено',
              'pic.required' => 'Следует загрузить картинку',
              'pic.image' => 'Выбранный файл не является изображением'
          );

          $validation = Validator::make($request->all(), $rules, $messages);
          #dd($request->file('pic'));
          if ($validation->fails()) {
            $message = $validation->errors()->first();
            return view('admin.slidersEdit', [
                'slider' => $slider,
                'message' => $message
                ]);
          } else {
              $slider->name = $request->all()['name'];
              $slider->link = $request->all()['link'];
              $slider->text = $request->all()['text'];
              if ($request->hasFile('pic')) {
                  $request->file('pic')->move(
                      storage_path() . '/app/public/uploads/sliders/slider-id-' . $slider->id,
                      'sliderPic.jpg'
                  );
                  $slider->pic = '/storage/uploads/sliders/slider-id-' . $slider->id . '/sliderPic.jpg';
              }
              $slider->save();
              return redirect()->route('admin.sliders');
          }
      }
      #dd($request->all());
      return view('admin.slidersEdit', ['slider' => $slider, 'message' => $message]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($slider_id)
    {
      $slider = Slider::find($slider_id);
      $slider->delete();
      return redirect()->route('admin.sliders');
    }
}
