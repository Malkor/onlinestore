<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

use App\Banner;
use App\Categorie;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::all();
        return view('admin.banners', ['banners' => $banners]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

            $message ='';
            $categories = Categorie::all();

            if (Route::currentRouteName() == 'admin.banners.create_post') {
                $rules = array(
                    'name' => 'required',
                    'pic' => 'required|image'
                );

                $messages = array(
                    'name.required' => 'Название должно быть заполнено',
                    'pic.required' => 'Следует загрузить картинку',
                    'pic.image' => 'Выбранный файл не является изображением'
                );

                $validation = Validator::make($request->all(), $rules, $messages);
                //dd($request->file('pic'));
                if ($validation->fails()) {
                    $message = $validation->errors()->first();
                    return view('admin.bannersCreate', ['categories' => $categories, 'message' => $message]);
                } else {
                    $banner = new Banner();
                    $banner->name = $request->all()['name'];
                    $banner->link = $request->all()['link'];
                    $banner->categorie_id = $request->all()['categorie_id'];
                    $banner->save();

                    Storage::makeDirectory('/uploads/banners/banner-id-' . $banner->id);

                    $request->file('pic')
                        ->move(storage_path() . '/app/public/uploads/banners/banner-id-' . $banner->id, 'bannerPic.jpg');

                    $banner->pic = '/storage/uploads/banners/banner-id-' . $banner->id . '/bannerPic.jpg';
                    $banner->save();
                    return redirect()->route('admin.banners');
                }
            }

            return view('admin.bannersCreate', ['message' => $message, 'categories' => $categories]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($banner_id, Request $request)
    {
        $banner = Banner::find($banner_id);
        $categories = Categorie::all();

        $message ='';
        if (Route::currentRouteName() == 'admin.banners.edit_post') {
            $rules = array(
              'name' => 'required',
              'pic' => 'image'
            );

            $messages = array(
              'name.required' => 'Название должно быть заполнено',
              'pic.required' => 'Следует загрузить картинку',
              'pic.image' => 'Выбранный файл не является изображением'
            );

            $validation = Validator::make($request->all(), $rules, $messages);
            #dd($request->new);
            if ($validation->fails()) {
                $message = $validation->errors()->first();
                return view('admin.bannersEdit', [
                    'banner' => $banner,
                    'categories' => $categories,
                    'message' => $message
                    ]);
            } else {
                $banner->name = $request->all()['name'];
                $banner->link = $request->all()['link'];
                $banner->categorie_id = $request->all()['categorie_id'];


                if ($request->hasFile('pic')) {
                    $request->file('pic')->move(
                        storage_path() . '/app/public/uploads/banners/banner-id-' . $banner->id,
                        'bannerPic.jpg'
                    );
                    $banner->pic = '/storage/uploads/banners/banner-id-' . $banner->id . '/bannerPic.jpg';
                }
                $banner->save();
                return redirect()->route('admin.banners');
            }
        }

        return view('admin.bannersEdit', ['banner' => $banner, 'categories' => $categories, 'message' => $message]);


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
