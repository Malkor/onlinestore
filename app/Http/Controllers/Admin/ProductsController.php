<?php

namespace App\Http\Controllers\Admin;

use App\Categorie;
use App\Http\Controllers\Controller;
use App\Product;
use App\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class ProductsController extends Controller
{
    public function index()
    {

        $products = Product::orderBy('created_at', 'desc')->get();
        return view('admin.products', ['products' => $products]);
    }

    public function create(Request $request)
    {
        $langs = Language::all();
        $categories = Categorie::all();
        $user = Auth::user()->id;
        #dd($user);
        $message ='';
        if (Route::currentRouteName() == 'admin.products.create_post') {
            $rules = array(
                'name' => 'required',
                'description' => 'required',
                'price' => 'required|numeric',
                'pic' => 'required|image'
            );

            $messages = array(
                'name.required' => 'Название должно быть заполнено',
                'description.required' => 'Описание должно быть заполнено',
                'price.required' => 'Цена должна быть заполнена',
                'price.numeric' => 'Цена должна быть числовым значением',
                'pic.required' => 'Следует загрузить картинку',
                'pic.image' => 'Выбранный файл не является изображением'
            );

            $validation = Validator::make($request->all(), $rules, $messages);
            #dd($request->file('pic'));
            if ($validation->fails()) {
                $message = $validation->errors()->first();
                return view('admin.productsCreate', ['categories' => $categories, 'message' => $message]);
            } else {
              #dd($request->all());

                $product = new Product();
                if(!empty($request->all()['active'])){
                  if($request->all()['active'] == 1){
                    $product->active = true;
                  }
                }else{
                  $product->active = false;
                }
                $product->name = $request->all()['name'];
                $product->categorie_id = $request->all()['categorie_id'];
                $product->description = $request->all()['description'];
                $product->price = $request->all()['price'];
                $product->old_price = $request->all()['old_price'];
                $product->user_id = $user;
                $product->lang_id = $request->all()['lang_id'];
                $product->currency_id = 1;
                $product->save();

                Storage::makeDirectory('/uploads/products/prod-id-' . $product->id);
                #dd($request->file('pic'));
                $request->file('pic')
                    ->move(storage_path() . '/app/public/uploads/products/prod-id-' . $product->id, 'productPic.jpg');

                $product->pic = '/storage/uploads/products/prod-id-' . $product->id . '/productPic.jpg';
                $product->save();
                return redirect()->route('admin.products');
            }
        }

        return view('admin.productsCreate', ['categories' => $categories, 'message' => $message, 'langs' => $langs]);
    }

    public function edit($product_id, Request $request)
    {
        $product = Product::find($product_id);
        $categories = Categorie::all();
        $langs = Language::all();
        $message ='';
        if (Route::currentRouteName() == 'admin.products.edit_post') {
            $rules = array(
                'name' => 'required',
                'description' => 'required',
                'price' => 'required|numeric',
                'pic' => 'image'
            );

            $messages = array(
                'name.required' => 'Название должно быть заполнено',
                'description.required' => 'Описание должно быть заполнено',
                'price.required' => 'Цена должна быть заполнена',
                'price.numeric' => 'Цена должна быть числовым значением',
                'pic.image' => 'Выбранный файл не является изображением'
            );

            $validation = Validator::make($request->all(), $rules, $messages);
            #dd($request->new);
            if ($validation->fails()) {
                $message = $validation->errors()->first();
                return view('admin.productsEdit', [
                    'product' => $product,
                    'categories' => $categories,
                    'message' => $message
                    ]);
            } else {
              #dd($request->all());
                if(!empty($request->all()['active'])){
                  if($request->active == 'on'){
                      $product->active = 1;
                  }
                }else{
                  $product->active = false;
                }
                $product->name = $request->all()['name'];
                $product->description = $request->all()['description'];
                $product->price = $request->all()['price'];
                $product->old_price = $request->all()['old_price'];
                $product->categorie_id = $request->all()['categorie_id'];

                if($request->new == 'on'){
                  $product->new = 1;
                }else{
                  $product->new = 0;
                }
                if($request->top == 'on'){
                  $product->top = 1;
                }else{
                  $product->top = 0;
                }
                $product->lang_id = $request->all()['lang_id'];

                if ($request->hasFile('pic')) {
                    $request->file('pic')->move(
                        storage_path() . '/app/public/uploads/products/prod-id-' . $product->id,
                        'productPic.jpg'
                    );
                    $product->pic = '/storage/uploads/products/prod-id-' . $product->id . '/productPic.jpg';
                }
                $product->save();
                return redirect()->route('admin.products');
            }
        }

        return view('admin.productsEdit', ['product' => $product, 'categories' => $categories, 'message' => $message, 'langs' => $langs]);
    }


    public function delete($product_id)
    {

        $product = Product::find($product_id);
        $product->delete();
        return redirect()->route('admin.products');
    }
}
