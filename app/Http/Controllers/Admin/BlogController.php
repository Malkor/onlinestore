<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BlogPost;
use App\Language;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    public function index()
    {
      $pagination = BlogPost::paginate(10);
      return view('admin.posts', ['posts' => $pagination]);
    }

    public function create(Request $request)
    {
        $message ='';
        $langs = Language::all();
        if (Route::currentRouteName() == 'admin.posts.create_post') {
            $rules = array(
                'title' => 'required',
                'description' => 'required'
            );

            $messages = array(
                'title.required' => 'Название должно быть заполнено',
                'description.required' => 'Описание должно быть заполнено',
            );

            $validation = Validator::make($request->all(), $rules, $messages);
            if ($validation->fails()) {
                $message = $validation->errors()->first();
                return view('admin.postsCreate', ['message' => $message, 'langs' => $langs]);
            } else {
                #dd($request->all());
                $slug = Str::slug($request->all()['title'], '-');
                $post = new BlogPost();
                $post->title = $request->all()['title'];
                $post->description = $request->all()['description'];
                $post->excerpt = $request->all()['excerpt'];
                $post->slug = $slug;
                $post->tags = $request->all()['tags'];
                $post->lang_id = $request->all()['lang_id'];
                $post->save();

                Storage::makeDirectory('/uploads/posts/post-id-' . $post->id);
                #dd($request->file('pic'));
                $request->file('pic')
                    ->move(storage_path() . '/app/public/uploads/posts/post-id-' . $post->id, 'postPic.jpg');

                $post->pic = '/storage/uploads/posts/post-id-' . $post->id . '/postPic.jpg';
                $post->save();
                return redirect()->route('admin.posts');
            }
        }

        return view('admin.postsCreate', ['message' => $message, 'langs' => $langs]);
    }

    public function edit($post_id, Request $request)
    {
        $message ='';
        $langs = Language::all();
        $post = BlogPost::find($post_id);
        if (Route::currentRouteName() == 'admin.posts.edit_post') {
            $rules = array(
                'title' => 'required',
                'description' => 'required'
            );

            $messages = array(
                'title.required' => 'Название должно быть заполнено',
                'description.required' => 'Описание должно быть заполнено',
            );

            $validation = Validator::make($request->all(), $rules, $messages);
            if ($validation->fails()) {
                $message = $validation->errors()->first();
                return view('admin.postsCreate', ['message' => $message, 'post' => $post, 'langs' => $langs]);
            } else {
                #dd($request->all());
                $slug = Str::slug($request->all()['title'], '-');
                $post->title = $request->all()['title'];
                $post->description = $request->all()['description'];
                $post->excerpt = $request->all()['excerpt'];
                $post->slug = $slug;
                $post->tags = $request->all()['tags'];
                $post->lang_id = $request->all()['lang_id'];

                if ($request->hasFile('pic')) {
                    $request->file('pic')->move(storage_path() . '/app/public/uploads/posts/post-id-' . $news->id, 'postPic.jpg');
                    $news->thumbnail = '/storage/uploads/posts/post-id-' . $news->id . '/postPic.jpg';
                }
                $post->save();
                return redirect()->route('admin.posts');
            }
        }

        return view('admin.postsEdit', ['post' => $post, 'message' => $message, 'langs' => $langs]);
    }

    public function delete($post_id)
    {
        $post = BlogPost::find($post_id);
        $post->delete();

        return redirect()->route('admin.posts');
    }
}
