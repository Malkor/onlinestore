<?php

namespace App\Http\Controllers\Data\PaymentCard;

use App\Http\Requests\CreateCardRequest;
use App\Http\Requests\UpdateCardRequest;
use App\PaymentCard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class PaymentCardController extends Controller
{
  public function cardList()
  {
    $user = JWTAuth::parseToken()->authenticate();
    return response()->json(['list' => PaymentCard::where('user_id', $user->id)->get()], 200);
  }

  public function addCard(CreateCardRequest $request)
  {
    $request_data = $request->all();
    $user = JWTAuth::parseToken()->authenticate();
    $request_data['user_id'] = $user->id;

    $new_card = new PaymentCard($request_data);

    $new_card->save();
    return response()->json(['item' => $new_card, 200]);
  }

  public function updateCard(UpdateCardRequest $request)
  {
    $item = PaymentCard::findOrFail($request->get('id'));

    is_null($request->get('owner')) ?: $item->owner = $request->get('owner');
    is_null($request->get('card_number')) ?: $item->card_number = $request->get('card_number');

    $item->save();

    return response()->json(['list' => $item, 200]);
  }
}
