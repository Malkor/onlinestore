<?php

namespace App\Http\Controllers\Data\Slider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Slider;

class SliderController extends Controller
{
    public function index()
    {
      $sliders = Slider::all();
      return response()->json(['sliders' => $sliders], 200);
    }
}
