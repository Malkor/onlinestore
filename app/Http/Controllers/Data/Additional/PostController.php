<?php

namespace App\Http\Controllers\Data\Additional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BlogPost;
use JWTAuth;

class PostController extends Controller
{
    public function getPosts()
    {
      $user = JWTAuth::parseToken()->authenticate();
      $userLang = $user->lang_id;
      $getPosts = BlogPost::where('lang_id', '=', $userLang)->get();
      return response()->json(['posts' => $getPosts, 'choose_lang' => $userLang], 200);
    }

    public function showBySlug($slug, Request $request)
    {
      $getPost = BlogPost::where('slug', $slug) -> first();
      return response()->json(['post' => $getPost, 'slug' => $slug], 200);
    }
}
