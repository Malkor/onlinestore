<?php

namespace App\Http\Controllers\Data\Additional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use App\PublicMenu;

class PublicMenuController extends Controller
{
    public function getMenu()
    {
      $user = JWTAuth::parseToken()->authenticate();
      $menu = PublicMenu::where('lang_id', $user->lang_id) -> get();
      return response()->json(['menu' => $menu], 200);
    }
}
