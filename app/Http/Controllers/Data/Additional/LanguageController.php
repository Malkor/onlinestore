<?php

namespace App\Http\Controllers\Data\Additional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Language;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use JWTAuth;

class LanguageController extends Controller
{
  //api return
    public function langList()
    {
      $langs = Language::all();
      return response()->json(['langs' => $langs], 200);
    }

    public function conditionVals($condition, Request $request)
    {
      $user = JWTAuth::parseToken()->authenticate();
      $userLang = $user->lang_id;
      $data = json_decode(file_get_contents(storage_path() . '/langs/' . $userLang . '/' . $condition . '.json'));
      return response()->json(['data' => $data->text], 200);
    }




//for admin panel
    public function list()
    {
      $langs = Language::all();
      $user = Auth::user();
      $userLang = $user->lang_id;
      $fileList = (storage_path() . '/langs/' . $userLang . '/');
      $files = scandir($fileList);
      foreach($files as $file){
        if($file != '.' && $file != '..'){
          $expFile = explode('.json', $file);
          $fileArray[] = $expFile[0];
        }
      }
      return view('admin.test', ['files' => $fileArray, 'langs' => $langs]);
    }

    public function edit($condition, Request $request)
    {
      $user = Auth::user();
      $langs = Language::all();
      $userLang = $user->lang_id;
      $data = json_decode(file_get_contents(storage_path() . '/langs/' . $userLang . '/' . $condition . '.json'));
      if (Route::currentRouteName() == 'admin.test.edit_post') {
        $user->lang_id = $request->all()['lang_id'];
        $user->save();
        $updateText = $request->all()['text'];
        $data->text = $updateText;
        file_put_contents(storage_path() . '/langs/' . $request->all()['lang_id'] . '/' . $condition . '.json', json_encode($data));
      }
      return view('admin.testCreate', ['data' => $data, 'condition' => $condition, 'langs' => $langs]);
    }




}
