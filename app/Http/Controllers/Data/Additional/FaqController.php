<?php

namespace App\Http\Controllers\Data\Additional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use App\Faq;

class FaqController extends Controller
{
  public function getFaq()
  {
    $user = JWTAuth::parseToken()->authenticate();
    $faq = Faq::where('lang_id', $user->lang_id) -> get();
    return response()->json(['faq' => $faq], 200);
  }
}
