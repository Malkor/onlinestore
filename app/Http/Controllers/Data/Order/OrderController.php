<?php

namespace App\Http\Controllers\Data\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use JWTAuth;

use App\Order;
use App\OrderPosition;


class OrderController extends Controller
{
    public function list(Request $request)
    {
      $this->user = JWTAuth::parseToken()->authenticate();
      $orderList = Order::where('user_id', '=', $this->user->id)->get();
      return response()->json(['orders' => $orderList, 'user' => $this->user]);
    }

    public function showOrder($order_id)
    {
      $order = Order::findOrFail($order_id);
      return response()->json(['order' => $order], 200);

    }


    public function createOrder(Request $request)
    {
      $user = JWTAuth::parseToken()->authenticate();
      $rules = array(
          'name' => 'required',
          'email' => 'required|email'
      );

      $messages = array(
          'name.required' => 'Укажите Ваше имя',
          'email.required' => 'Укажите Ваш Email',
          'email.email' => 'Некорректный формат Email'
      );

      $validation = Validator::make($request->all(), $rules, $messages);

      if ($validation->fails()) {
          $message = $validation->errors()->first();
          $success = 0;
      } else {
          try {
              $order = $this->user->orders()->where('status', '=', 0)->first();
              $order->status = 1;
              $order->user_id = $user->id;
              $order->customer_name = $customerName;
              $order->customer_email = $customerEmail;
              $order->save();

              event(new OrderPostedEvent($order));
              $success = 1;
              $message = 'Спасибо за заказ. Наш менеджер свяжется с Вами в течение дня.';
          } catch (\Exception $e) {
              $success = 0;
              $message = 'Произошла непредвиденная ошибка. Попробуйте позже или свяжитесь с администратором';
              Log::error($e->getMessage(), ['exception' => $e]);
          }
      }

      $response = json_encode(['message' => $message, 'success' => $success], JSON_UNESCAPED_UNICODE);

      return $response;
    }

}
