<?php

namespace App\Http\Controllers\Data\Categories;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categorie;
use App\Product;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $categories = Categorie::all();

      return response()->json(['categories' => $categories], 200);
    }



    public function show($category_id)
    {
        $categoryData = Categorie::find($category_id);
        $categoryElements =  Product::where('categorie_id', $category_id)->get();

        return response()->json(compact('categoryData', 'categoryElements'));
    }

}
