<?php

namespace App\Http\Controllers\Data\Banner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Banner;

class BannerController extends Controller
{
    public function index()
    {
      $products = Banner::all();
      return response()->json(['banners' => $products], 200);
    }
}
