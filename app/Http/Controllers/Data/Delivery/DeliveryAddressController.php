<?php

namespace App\Http\Controllers\Data\Delivery;

use App\DeliveryAddress;
use App\Http\Requests\DeliveryAddressCreateRequest;
use App\Http\Requests\DeliveryAddressUpdateRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class DeliveryAddressController extends Controller
{
  public function getDeliveryList(Request $request)
  {
    $user = JWTAuth::parseToken()->authenticate();
    return response()->json(['list' => DeliveryAddress::where('user_id', $user->id)->get()], 200);
  }

  public function addDeliveryAddress(DeliveryAddressCreateRequest $request)
  {
    $user = JWTAuth::parseToken()->authenticate();
    $request_data = $request->all();
    $request_data['user_id'] = $user->id;

    $new_delivery = new DeliveryAddress($request_data);
    $new_delivery->save();

    return response()->json(['item' => $request_data], 200);
  }

  public function updateDeliveryAddress(DeliveryAddressUpdateRequest $request)
  {
    $item = DeliveryAddress::findOrFail($request->get('id'));

    is_null($request->get('address_title')) ?: $item->address_title = $request->get('address_title');
    is_null($request->get('first_name')) ?: $item->first_name = $request->get('first_name');
    is_null($request->get('last_name')) ?: $item->last_name = $request->get('last_name');
    is_null($request->get('company')) ?: $item->company = $request->get('company');
    is_null($request->get('address')) ?: $item->address = $request->get('address');
    is_null($request->get('address_line_2')) ?: $item->address_line_2 = $request->get('address_line_2');
    is_null($request->get('country')) ?: $item->country = $request->get('country');
    is_null($request->get('state')) ?: $item->state = $request->get('state');
    is_null($request->get('city')) ?: $item->city = $request->get('city');
    is_null($request->get('postal_code')) ?: $item->postal_code = $request->get('postal_code');
    is_null($request->get('home_phone')) ?: $item->home_phone = $request->get('home_phone');
    is_null($request->get('mobile_phone')) ?: $item->mobile_phone = $request->get('mobile_phone');
    is_null($request->get('additional_information')) ?: $item->additional_information = $request->get('additional_information');

    $item->save();
    return response()->json(['item' => $item], 200);

  }
}
