<?php

namespace App\Http\Controllers\Data\Product;

use App\Currency;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use JWTAuth;

class ProductController extends Controller
{
  /// TODO перенести в .env
  public $default_currency = 'USD';

  public function index()
  {
    $products = Product::all();
    if ($this->currency !== $this->default_currency) {
      $course = (Currency::where('code', $this->currency)->get())[0]->current_value;
      foreach ($products as &$el) {
        $el->price = round($el->price * $course);
        $el->old_price = round($el->old_price * $course);
      }
    }
    return response()->json(['products' => $products], 200);
  }

  public function topProducts()
  {
    $topProduct = Product::where('top', 1)->get();
    return response()->json(['top_products' => $topProduct], 200);
  }

  public function newProducts()
  {
    $newProduct = Product::where('new', 1)->get();
    return response()->json(['new_products' => $newProduct], 200);
  }

  public function show($product_id)
  {
    $showProduct = Product::findOrFail($product_id);
    return response()->json(['product' => $showProduct]);
  }
}
