<?php

namespace App\Http\Controllers\Data\Currencies;

use App\Currency;
use App\Http\Controllers\Controller;

class CurrenciesController extends Controller
{
  public function getCurrenciesList()
  {
    $news = Currency::all();
    return response()->json(['list' => $news, 200]);
  }
}
