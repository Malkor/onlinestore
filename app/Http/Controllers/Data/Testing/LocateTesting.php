<?php


namespace App\Http\Controllers\Data\Testing;


use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;


class LocateTesting extends Controller
{
  public function index(Request $request)
  {
    return new JsonResponse([
      'lang' => $this->lang,
      'cur' => $this->currency,
      'user' => $this->user
    ], Response::HTTP_OK);
  }
}