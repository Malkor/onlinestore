<?php

namespace App\Http\Controllers\Data\Support;

use App\Contacts;
use App\Http\Controllers\Controller;
use App\SocialNetworks;

class SupportDataController extends Controller
{
  public function getSocialList()
  {
    $categories = SocialNetworks::all();
    return response()->json(['categories' => $categories], 200);
  }

  public function getContactData()
  {
    $categories = Contacts::all();
    return response()->json(['categories' => $categories], 200);
  }
}
