<?php

namespace App\Http\Controllers\Data\Menu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MenuItem;
use DB;
class MenuController extends Controller
{
    public function information()
    {
      $infoList = DB::table('menu_items')->where('menu_id', '=', 3)->get();
      return response()->json(['info' => $infoList], 200);
    }

    public function account()
    {
      $account = DB::table('menu_items')->where('menu_id', '=', 4)->get();
      return response()->json(['account' => $account], 200);
    }
}
