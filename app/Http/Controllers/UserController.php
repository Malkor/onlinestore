<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\PayLoadFactory;
use Tymon\JWTAuth\JWTManager as JWT;


class UserController extends Controller
{
    public function register(Request $request)
    {

      $validator = Validator::make($request->all(), [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users',
        'password' => 'required|string|min:6',
        //'phone' => 'required|string|max:120',
      ]);

      //if($validator->fails()){
        //return response()->json($request->json(), 400);
      //}

      $user = User::create([
        'name' => $request->name,
        'email' => $request->email,
        'password' => Hash::make($request->password),
        'phone' => $request->phone,
      ]);

      $token = JWTAuth::fromUser($user);
      $user->save();
      return response()->json(compact('user', 'token'), 201);
      //return response()->json($request->json());
    }


    public function login(Request $request)
    {
      $credentials = $request->all();
      try {
        if(!$token = JWTAuth::attempt($credentials)){
          return response()->json(['error' => 'invalid'], 400);
        }
      } catch (JWTException $e) {
          return response()->json(['error' => 'couldnt create token'], 500);
      }

      return response()->json(compact('token'));
    }



    public function getAuthentificatedUser()
    {
      $user = JWTAuth::parseToken();

      try {
          if (! $user = JWTAuth::parseToken()->authenticate()) {
              return response()->json(['user_not_found'], 404);
          }
      } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
          return response()->json(['token_expired'], $e->getStatusCode());
      } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
          return response()->json(['token_invalid'], $e->getStatusCode());
      } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
          return response()->json(['token_absent'], $e->getStatusCode());
      }
      return response()->json(compact('user'));
    }


}
