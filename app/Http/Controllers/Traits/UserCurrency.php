<?php


namespace App\Http\Controllers\Traits;


use Illuminate\Http\Request;

trait UserCurrency
{
  protected function getUserCurrency(Request $request)
  {
    if ($request->header('Content-UserLanguage')) {
      return $request->header('Content-UserLanguage');
    }
  }
}