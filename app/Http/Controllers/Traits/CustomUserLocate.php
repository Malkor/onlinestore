<?php


namespace App\Http\Controllers\Traits;


use Illuminate\Http\Request;

trait CustomUserLocate
{
  protected $locate;
  private $default_lang = 'en';
  private $lang_list = ['en', 'ru', 'fr', 'es', 'it', 'pt', 'cn', 'de', 'dk'];

  private function getLocate(Request $request)
  {
    if ($request->header('Content-Language')) {
      return $request->header('Content-Language');
    }
    $api_locate = $this->getLocateByIP($request);
    return in_array($api_locate, $this->lang_list) ? $api_locate : $this->default_lang;
  }

  private function getLocateByIP(Request $request)
  {
    try {
      $ch = curl_init("http://api.sypexgeo.net/json/" . $request->getClientIp());
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_HEADER, false);
      $locate_api = curl_exec($ch);
      curl_close($ch);
      $res = json_decode($locate_api, true);

      if (is_null($res)) {
        return $this->default_lang;
      }

      return mb_strtolower($res['country']['iso']);

    } catch (\Exception $e) {
      return $this->default_lang;
    }
  }
}