<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\Traits\CustomUserLocate;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use JWTAuth;

class Controller extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests, CustomUserLocate;

  /// TODO присвоить дефолтное значение из .env
  protected $currency = 'USD';

  protected $user;
  protected $lang = 'en';


  public function __construct(Request $request)
  {
//      $this->locate = $this->getLocate($request);
      $this->lang = $this->getUserLanguage($request);
      $this->currency = $this->getUserCurrency($request);

  }

  protected function getUserCurrency(Request $request)
  {
    if ($request->header('Currency')) {
      $this->currency = $request->header('Currency');
    }
    return $this->currency;
  }

  protected function getUserLanguage(Request $request)
  {
    if ($request->header('Content-Language')) {
     $this->lang = $request->header('Content-Language');
    }
    return $this->lang;
  }

}
