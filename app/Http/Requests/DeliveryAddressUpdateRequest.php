<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeliveryAddressUpdateRequest extends RestFormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'id' => 'required|integer|exists:delivery_addresses,id',
      'address_title' => 'string|min:3|max:60',
      'first_name' => 'string|min:3|max:60',
      'last_name' => 'string|min:3|max:60',
      'company' => 'string|min:3|max:60',
      'address' => 'string|min:3|max:90',
      'address_line_2' => 'string|min:3|max:90',
      'country' => 'string|min:3|max:60',
      'state' => 'string|min:3|max:60',
      'city' => 'string|min:3|max:60',
      'postal_code' => 'string|min:4|max:30',
      'home_phone' => 'string|min:3|max:30',
      'mobile_phone' => 'string|min:3|max:30',
      'additional_information' => 'string',
    ];
  }
}
