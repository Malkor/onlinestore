<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeliveryAddressCreateRequest extends RestFormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'address_title' => 'required|string|min:3|max:60',
      'first_name' => 'required|string|min:3|max:60',
      'last_name' => 'required|string|min:3|max:60',
      'company' => 'string|min:3|max:60',
      'address' => 'required|string|min:3|max:90',
      'address_line_2' => 'required|string|min:3|max:90',
      'country' => 'required|string|min:3|max:60',
      'state' => 'required|string|min:3|max:60',
      'city' => 'required|string|min:3|max:60',
      'postal_code' => 'required|string|min:4|max:30',
      'home_phone' => 'required|string|min:3|max:30',
      'mobile_phone' => 'required|string|min:3|max:30',
      'additional_information' => 'required|string',
    ];
  }
}
