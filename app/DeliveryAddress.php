<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static findOrFail($id)
 */
class DeliveryAddress extends Model
{
  protected $fillable = [
    'user_id',
    'address_title',
    'first_name',
    'last_name',
    'company',
    'address',
    'address_line_2',
    'country',
    'state',
    'city',
    'postal_code',
    'home_phone',
    'mobile_phone',
    'additional_information',
  ];
}
