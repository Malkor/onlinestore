<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('/public/categories')->group(function () {
  Route::get('list', 'Data\Categories\CategoryController@index');
  Route::get('/{id}', 'Data\Categories\CategoryController@show');

});


Route::post('login', 'Auth\ApiController@login');
Route::post('register', 'Auth\ApiController@register');

Route::group(['prefix' => ''], function () {
  Route::get('logout', 'Auth\ApiController@logout');
  Route::get('user', 'Auth\ApiController@getAuthUser');
});

Route::group(['prefix' => '/public/menu'], function () {
  Route::post('{condition}', 'Data\Additional\LanguageController@conditionVals');
});

Route::group(['prefix' => '/public/blog'], function () {
  Route::get('list', 'Data\Additional\PostController@getPosts');
  Route::post('{slug}', 'Data\Additional\PostController@showBySlug');
});

Route::prefix('/public/langs/')->group(function () {
  Route::get('list', 'Data\Additional\LanguageController@langList');
  Route::post('set_lang', 'Auth\ApiController@setLang');
});

Route::prefix('/public/menu/')->group(function () {
  Route::get('information', 'Data\Menu\MenuController@information')->name('menu.information');
  Route::get('account', 'Data\Menu\MenuController@account')->name('menu.account');
});

Route::prefix('/public/products')->group(function () {
  Route::get('list', 'Data\Product\ProductController@index')->name('product.list');
  Route::get('top', 'Data\Product\ProductController@topProducts')->name('product.top');
  Route::get('new', 'Data\Product\ProductController@newProducts')->name('product.new');
  Route::get('show/{id}', 'Data\Product\ProductController@show')->name('product.show');
});


Route::prefix('/public/order')->group(function () {
  Route::post('list', 'Data\Order\OrderController@list');
  Route::get('show/{id}', 'Data\Order\OrderController@showOrder');
});

Route::prefix('/public/sliders')->group(function () {
  Route::get('list', 'Data\Slider\SliderController@index');
});

//banners
Route::prefix('/public/banners')->group(function () {
  Route::get('list', 'Data\Banner\BannerController@index');
});

//cart
Route::group(['prefix' => 'cart', 'middleware' => 'registered'], function () {
    Route::get('/', 'CartController@index');
    Route::get('/add/{product_id}', 'CartController@add');
    Route::get('/delete/{orderPosition_id}', 'CartController@delete');
    Route::post('/send', 'CartController@send');
});

//delivery
Route::group(['prefix' => 'user/delivery'], function () {
  Route::get('/list', 'Data\Delivery\DeliveryAddressController@getDeliveryList')->name('delivery.list');
  Route::post('/add', 'Data\Delivery\DeliveryAddressController@addDeliveryAddress')->name('delivery.add');
  Route::post('/update', 'Data\Delivery\DeliveryAddressController@updateDeliveryAddress')->name('delivery.update');
});

Route::group(['prefix' => 'user/card'], function () {
  Route::get('/list', 'Data\PaymentCard\PaymentCardController@cardList')->name('card.list');
  Route::post('/add', 'Data\PaymentCard\PaymentCardController@addCard')->name('card.add');
  Route::post('/update', 'Data\PaymentCard\PaymentCardController@updateCard')->name('card.update');

});


Route::prefix('/public/testing/locate')->group(function () {
  Route::get('/', 'Data\Testing\LocateTesting@index')->name('test.locate');
  Route::get('/cur', 'Admin\UpdateCurrency@saveCurrenciesValue')->name('test.currency');
  Route::get('/new_cur', 'Admin\UpdateCurrency@newCurr')->name('test.new_currency');

});

Route::prefix('/public/currency')->group(function () {
  Route::get('/list', 'Data\Currencies\CurrenciesController@getCurrenciesList')->name('currency.list');
});

Route::prefix('/public/support/data')->group(function () {
  Route::get('/social', 'Data\Support\SupportDataController@getSocialList')->name('support.social');
  Route::get('/contacts', 'Data\Support\SupportDataController@getContactData')->name('support.contacts');
});


Route::group(['prefix' => '/public/menu'], function () {
  Route::get('/all', 'Data\Additional\PublicMenuController@getMenu');
});

Route::group(['prefix' => '/public/faq'], function () {
  Route::get('/list', 'Data\Additional\FaqController@getMenu');
});
