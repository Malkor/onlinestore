<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/search', 'SearchController@index')->name('search');

Route::get('/categories/{categorie_id}', 'CategoriesController@index')->name('categories');

Route::get('/product/{product_id}', 'ProductController@index')->name('product');

Route::get('/about', 'AboutController@index')->name('about');

Route::get('/news', 'NewsController@index')->name('news');
Route::get('/news/article/{news_id}', 'NewsController@article')->name('news.article');

Route::group(['prefix' => 'cart', 'middleware' => 'registered'], function () {
    Route::get('/', 'CartController@index')->name('cart');
    Route::get('/add/{product_id}', 'CartController@add')->name('cart.add');
    Route::get('/delete/{orderPosition_id}', 'CartController@delete')->name('cart.delete');
    Route::post('/send', 'CartController@send')->name('cart.send');
});

Route::get('/orders', 'MyOrdersController@index')->name('myOrders')->middleware('registered');

Route::group(['prefix' => 'admin', 'middleware' => 'adminOnly'], function () {

    Route::get('/', function () {
        return redirect()->route('admin.settings');
    })->name('admin');

    Route::get('/settings', 'Admin\SettingsController@index')->name('admin.settings');
    Route::post('/settings/send', 'Admin\SettingsController@send')->name('admin.settings.send');

    Route::get('/categories', 'Admin\CategoriesController@index')->name('admin.categories');
    Route::get('/categories/create', 'Admin\CategoriesController@create')->name('admin.categories.create');
    Route::post('/categories/create', 'Admin\CategoriesController@create')->name('admin.categories.create_post');
    Route::get('/categories/edit/{categorie_id}', 'Admin\CategoriesController@edit')->name('admin.categories.edit');
    Route::post('/categories/edit/{categorie_id}', 'Admin\CategoriesController@edit')->name('admin.categories.edit_post');
    Route::get('/categories/delete/{categorie_id}', 'Admin\CategoriesController@delete')->name('admin.categories.delete');

    Route::get('/products', 'Admin\ProductsController@index')->name('admin.products');
    Route::get('/products/create', 'Admin\ProductsController@create')->name('admin.products.create');
    Route::post('/products/create', 'Admin\ProductsController@create')->name('admin.products.create_post');
    Route::get('/products/edit/{product_id}', 'Admin\ProductsController@edit')->name('admin.products.edit');
    Route::post('/products/edit/{product_id}', 'Admin\ProductsController@edit')->name('admin.products.edit_post');
    Route::get('/products/delete/{product_id}', 'Admin\ProductsController@delete')->name('admin.products.delete');

    Route::get('/orders', 'Admin\OrdersController@index')->name('admin.orders');
    Route::get('/orders/view/{order_id}', 'Admin\OrdersController@view')->name('admin.orders.view');

    Route::get('/news', 'Admin\NewsController@index')->name('admin.news');
    Route::get('/news/create', 'Admin\NewsController@create')->name('admin.news.create');
    Route::post('/news/create', 'Admin\NewsController@create')->name('admin.news.create_post');
    Route::get('/news/edit/{news_id}', 'Admin\NewsController@edit')->name('admin.news.edit');
    Route::post('/news/edit/{news_id}', 'Admin\NewsController@edit')->name('admin.news.edit_post');
    Route::get('/news/delete/{news_id}', 'Admin\NewsController@delete')->name('admin.news.delete');

    Route::get('/users', 'Admin\UserList@index')->name('admin.users');
    Route::get('/users/edit/{user_id}', 'Admin\UserList@edit')->name('admin.users.edit');
    Route::post('/users/edit/{user_id}', 'Admin\UserList@edit')->name('admin.users.edit_post');
    Route::get('/users/delete/{user_id}', 'Admin\UserList@delete')->name('admin.users.delete');

    Route::get('/currencies', 'Admin\CurrencyList@index')->name('admin.currencies');
    Route::get('/currencies/create', 'Admin\CurrencyList@create')->name('admin.currencies.create');
    Route::post('/currencies/create', 'Admin\CurrencyList@create')->name('admin.currencies.create_post');
    Route::get('/currencies/edit/{banner_id}', 'Admin\CurrencyList@edit')->name('admin.currencies.edit');
    Route::post('/currencies/edit/{banner_id}', 'Admin\CurrencyList@edit')->name('admin.currencies.edit_post');

    Route::get('/banners', 'Admin\BannerController@index')->name('admin.banners');
    Route::get('/banners/create', 'Admin\BannerController@create')->name('admin.banners.create');
    Route::post('/banners/create', 'Admin\BannerController@create')->name('admin.banners.create_post');
    Route::get('/banners/edit/{banner_id}', 'Admin\BannerController@edit')->name('admin.banners.edit');
    Route::post('/banners/edit/{banner_id}', 'Admin\BannerController@edit')->name('admin.banners.edit_post');
    Route::get('/news/delete/{banner_id}', 'Admin\BannerController@delete')->name('admin.banners.delete');

    Route::get('/sliders', 'Admin\SliderController@index')->name('admin.sliders');
    Route::get('/sliders/create', 'Admin\SliderController@create')->name('admin.sliders.create');
    Route::post('/sliders/create', 'Admin\SliderController@create')->name('admin.sliders.create_post');
    Route::get('/sliders/edit/{slider_id}', 'Admin\SliderController@edit')->name('admin.sliders.edit');
    Route::post('/sliders/edit/{slider_id}', 'Admin\SliderController@edit')->name('admin.sliders.edit_post');
    Route::get('/sliders/delete/{slider_id}', 'Admin\SliderController@delete')->name('admin.sliders.delete');

    Route::get('/update_info/list/', 'Data\Additional\LanguageController@list')->name('admin.test');
    Route::get('/update_info/edit/{condition}', 'Data\Additional\LanguageController@edit')->name('admin.test.edit');
    Route::post('/update_info/edit/{condition}', 'Data\Additional\LanguageController@edit')->name('admin.test.edit_post');


    Route::get('/faqs', 'Admin\FaqController@list')->name('admin.faqs');
    Route::get('/faqs/create', 'Admin\FaqController@create')->name('admin.faqs.create');
    Route::post('/faqs/create', 'Admin\FaqController@create')->name('admin.faqs.create_post');
    Route::get('/faqs/edit/{faq_id}', 'Admin\FaqController@edit')->name('admin.faqs.edit');
    Route::post('/faqs/edit/{faq_id}', 'Admin\FaqController@edit')->name('admin.faqs.edit_post');
    #Route::get('/faqs/delete/{faq_id}', 'Admin\FaqController@delete')->name('admin.faqs.delete');

    Route::get('/posts', 'Admin\BlogController@index')->name('admin.posts');
    Route::get('/posts/create', 'Admin\BlogController@create')->name('admin.posts.create');
    Route::post('/posts/create', 'Admin\BlogController@create')->name('admin.posts.create_post');
    Route::get('/posts/edit/{post_id}', 'Admin\BlogController@edit')->name('admin.posts.edit');
    Route::post('/posts/edit/{post_id}', 'Admin\BlogController@edit')->name('admin.posts.edit_post');
    Route::get('/posts/delete/{post_id}', 'Admin\BlogController@delete')->name('admin.posts.delete');


    Route::get('/menus', 'Admin\PublicMenuController@index')->name('admin.menus');
    Route::get('/menus/create', 'Admin\PublicMenuController@create')->name('admin.menus.create');
    Route::post('/menus/create', 'Admin\PublicMenuController@create')->name('admin.menus.create_post');
    Route::get('/menus/edit/{post_id}', 'Admin\PublicMenuController@edit')->name('admin.menus.edit');
    Route::post('/menus/edit/{post_id}', 'Admin\PublicMenuController@edit')->name('admin.menus.edit_post');


});
